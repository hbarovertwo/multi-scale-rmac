import os
import h5py
import numpy as np
from numba import njit
from keras.applications.vgg16 import VGG16
from keras.applications.vgg16 import preprocess_input
from keras.preprocessing.image import load_img, img_to_array
from tqdm import tqdm
import keras.backend as K
from PIL import ImageFile

ImageFile.LOAD_TRUNCATED_IMAGES = True

model = VGG16(weights='imagenet')
model.compile(optimizer='adam', loss='categorical_crossentropy')

from time import time

@njit
def Get_Window(H, W, Scale):
    width = 2*(min(H,W))//(Scale+1)
    scale = np.array([3,4,1,2,2,5])
    a,b = np.array([scale[2*(Scale-1)],scale[2*(Scale-1)+1]]) 
    height = a*(H//b)
    return height, width

@njit
def twenty_window_sum(L1,H1, W1,j):
    r_sum = np.empty((20,))
    # first scale (2 windows)
    win1 = Get_Window(L1.shape[1],L1.shape[2],1)
    r_sum[0] = np.max(L1[:,:win1[0],:win1[1],j])
    r_sum[1] = np.max(L1[:,H1-win1[0]:,:win1[1],j])
    # second scale (6 windows)
    win2 = Get_Window(L1.shape[1],L1.shape[2],2)
    r_sum[2] = np.max(L1[:,:win2[0],:win2[1],j])
    r_sum[3] = np.max(L1[:,:win2[0],W1-(win2[1]):,j])
    r_sum[4] = np.max(L1[:,(win2[0]//2):3*(win2[0]//2),:win2[1],j])
    r_sum[5] = np.max(L1[:,(win2[0]//2):3*(win2[0]//2),W1-(win2[1]):,j])
    r_sum[6] = np.max(L1[:,win2[0]:,:win2[1],j])
    r_sum[7] = np.max(L1[:,win2[0]:,W1-(win2[1]):,j])
    # third scale (12 windows)
    win3 = Get_Window(L1.shape[1],L1.shape[2],3)
    r_sum[8] = np.max(L1[:,:win3[0],:win3[1],j])
    r_sum[9] = np.max(L1[:,:win3[0],(win3[1]//2):3*(win3[1]//2),j])
    r_sum[10] = np.max(L1[:,:win3[0],win3[1]:,j])
    r_sum[11] = np.max(L1[:,(win3[0]//2):3*(win3[0]//2),:win3[1],j])
    r_sum[12] = np.max(L1[:,(win3[0]//2):3*(win3[0]//2),(win3[1]//2):3*(win3[1]//2),j])
    r_sum[13] = np.max(L1[:,(win3[0]//2):3*(win3[0]//2),win3[1]:,j])
    r_sum[14] = np.max(L1[:,win3[0]:2*win3[0],:win3[1],j])
    r_sum[15] = np.max(L1[:,win3[0]:2*win3[0],(win3[1]//2):3*(win3[1]//2),j])
    r_sum[16] = np.max(L1[:,win3[0]:2*win3[0],win3[1]:,j])
    r_sum[17] = np.max(L1[:,2*win3[0]:,:win3[1],j])
    r_sum[18] = np.max(L1[:,2*win3[0]:,(win3[1]//2):3*(win3[1]//2),j])
    r_sum[19] = np.max(L1[:,2*win3[0]:,win3[1]:,j])
    return np.sum(r_sum)

def preprocessing(input_image):
    img = load_img(input_image, target_size=(224,224))
    img = img_to_array(img)
    img = np.expand_dims(img, axis=0)
    img = preprocess_input(img)
    return img


db = '/home/rahul/new_vienna/'
with open('/home/rahul/similarity/trainset.txt','r') as f:
    im_files = eval(f.read())

files = [db+i for i in im_files]

hf = h5py.File('cache_v3.h5','w')

alpha = (0.06,0.13,0.81)

# custom backend function to get output of intermediate layers.
layers = K.function([model.layers[0].input],
                    [model.layers[9].output,
                     model.layers[13].output,
                     model.layers[17].output])

feature = np.empty([1280,])

for path in tqdm(files):
    #t1 = time()
    im = preprocessing(path)
    activations = layers([im])
    
    l = [activations[0], activations[1], activations[2]]
    #print('time1:', time()-t1)
    #feature = np.empty([1280,])
    #t2 = time()
    index = 0
    for q in range(0,3):
        a = alpha[q]
        for lyr in range(l[q].shape[3]):
            feature[index] = a*twenty_window_sum(l[q], l[q].shape[1],l[q].shape[2], lyr)
            index += 1
    #print('time2: ', time()-t2)
    #t3 = time()
    norm_feature = feature / np.sqrt(np.sum(feature**2))
    hf.create_dataset(path, data=norm_feature)
    #print('time3: ', time() - t3)
    
hf.close()
print('vectors stored in cache_v2.h5!')
