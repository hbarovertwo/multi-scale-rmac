import os
import numpy as np
from keract import get_activations
from keras.applications.vgg16 import VGG16
from keras.applications.vgg16 import preprocess_input
from keras.preprocessing.image import load_img, img_to_array
from tqdm import tqdm
from PIL import ImageFile
ImageFile.LOAD_TRUNCATED_IMAGES = True

model = VGG16(weights='imagenet')
model.compile(optimizer='adam', loss='categorical_crossentropy')

def preprocessing(input_image):
    img = load_img(input_image, target_size=(224,224))
    img = img_to_array(img)
    img = np.expand_dims(img, axis=0)
    img = preprocess_input(img)
    return img

db = '/home/rahul/new_vienna/'
with open('/home/rahul/similarity/trainset.txt','r') as f:
    im_files = eval(f.read())
files = [db+i for i in im_files]

from time import time
import matplotlib.pyplot as plt
times = []

for i in range(len(files)):
    a = time()
    im = preprocessing(files[i])
    activations = get_activations(model, im)
    times.append(time()-a)
    print(times[-1], 'seconds')
    if i % 100 == 0 and i > 0:
        plt.plot(times)
        plt.savefig('foo.png')
        print('figure saved')
